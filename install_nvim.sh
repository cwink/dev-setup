#!/bin/bash

echo '========================================='
echo '  Checking for required applications...  '
echo '========================================='

which apt 2>&1 1>/dev/null

if [ $? -ne 0 ]; then
  echo "Only Ubuntu is supported at the moment for dependencies." 1>&2 >&2

  exit 1
fi

which git 2>&1 1>/dev/null

if [ $? -ne 0 ]; then
  echo "Please install git." 1>&2 >&2

  exit 2
fi

echo '========================================='
echo '  Installing dependencies...             '
echo '========================================='

sudo apt update

if [ $? -ne 0 ]; then
  echo "Failed to update package respository." 1>&2 >&2

  exit 3
fi

sudo apt install -y ninja-build gettext libtool libtool-bin autoconf automake cmake g++ pkg-config unzip curl

if [ $? -ne 0 ]; then
  echo "Failed to install all dependencies." 1>&2 >&2

  exit 4
fi

rm -rf /usr/local/neovim

rm -rf neovim

echo '========================================='
echo '  Checking out repository...             '
echo '========================================='

git clone https://github.com/neovim/neovim.git

if [ $? -ne 0 ]; then
  echo "Failed to checkout neovim repository." 1>&2 >&2

  exit 5
fi

cd neovim/

rm -rf build

echo '========================================='
echo '  Building Neovim...                     '
echo '========================================='

make CMAKE_BUILD_TYPE=Release install

if [ $? -ne 0 ]; then
  echo "Failed to build Neovim." 1>&2 >&2

  exit 6
fi

cd ..

rm -rf neovim

echo '========================================='
echo '  Installation complete.                 '
echo '========================================='

exit 0
