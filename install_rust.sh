#!/bin/bash

echo '========================================='
echo '  Checking for required applications...  '
echo '========================================='

which curl 2>&1 1>/dev/null

if [ $? -ne 0 ]; then
  echo "Please install curl." 1>&2 >&2

  exit 1
fi

echo '========================================='
echo '  Installing rustup...                   '
echo '========================================='

curl --proto '=https' --tlsv1.2 -sSf --connect-timeout 10 https://sh.rustup.rs | sh -s -- -y

if [ $? -ne 0 ]; then
  echo "Failed to install rustup." 1>&2 >&2

  exit 2
fi

echo '========================================='
echo '  Installing required components...      '
echo '========================================='

rustup component add cargo clippy rls rust-analysis rust-src rustfmt

if [ $? -ne 0 ]; then
  echo "Failed to install rust components." 1>&2 >&2

  exit 3
fi

echo '========================================='
echo '  Installation complete.                 '
echo '========================================='

exit 0
