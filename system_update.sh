#!/bin/bash

echo '========================================='
echo '  Starting update...                     '
echo '========================================='

PACKAGE_MANAGER=''

which apt 2>&1 1>/dev/null >/dev/null

if [ $? -eq 0 ]; then
  PACKAGE_MANAGER='APT'
fi

which dnf 2>&1 1>/dev/null >/dev/null

if [ $? -eq 0 ]; then
  PACKAGE_MANAGER='DNF'
fi

which pacman 2>&1 1>/dev/null >/dev/null

if [ $? -eq 0 ]; then
  PACKAGE_MANAGER='PACMAN'
fi

start_update() {
  case "${PACKAGE_MANAGER}" in
    "APT")
      sudo apt -y update
  
      if [ $? -ne 0 ]; then
        echo 'Failed to run apt update.' 1>&2 >&2
  
        return 1
      fi
  
      sudo apt -y full-upgrade
  
      if [ $? -ne 0 ]; then
        echo 'Failed to run apt upgrade.' 1>&2 >&2
  
        return 2
      fi
  
      sudo apt -y update
  
      if [ $? -ne 0 ]; then
        echo 'Failed to run apt update.' 1>&2 >&2
  
        return 3
      fi
  
      sudo apt-get -y --purge autoremove
  
      if [ $? -ne 0 ]; then
        echo 'Failed to autoremove old applications.' 1>&2 >&2
  
        return 4
      fi
  
      sudo apt -y update
  
      if [ $? -ne 0 ]; then
        echo 'Failed to run apt update.' 1>&2 >&2
  
        return 5
      fi
  
      sudo apt -y full-upgrade
  
      if [ $? -ne 0 ]; then
        echo 'Failed to run apt upgrade.' 1>&2 >&2
  
        return 6
      fi
  
      ;;
  
    "DNF")
      sudo dnf update -y

      if [ $? -ne 0 ]; then
        echo 'Failed to run dnf upgrade.' 1>&2 >&2
  
        return 7
      fi

      ;;
  
    "PACMAN")
      sudo pacman -Syu

      if [ $? -ne 0 ]; then
        echo 'Failed to run pacman upgrade.' 1>&2 >&2
  
        return 8
      fi

      ;;
  
    *)
        echo 'Could not find a supported package manager.' 1>&2 >&2
       
        return 9
      ;;
  esac

  return 0
}

start_update

RESULT=$?

echo '========================================='
echo '  System update finished.                '
echo '========================================='

exit ${RESULT}
