#!/bin/bash

JDTLS_URL='http://download.eclipse.org/jdtls/snapshots/jdt-language-server-latest.tar.gz'

CPPTOOLS_URL='https://github.com/microsoft/vscode-cpptools/releases/download/1.6.0/cpptools-linux.vsix'

OPENJDK_URL='https://download.java.net/openjdk/jdk11/ri/openjdk-11+28_linux-x64_bin.tar.gz'

GRADLE_URL='https://downloads.gradle-dn.com/distributions/gradle-7.2-bin.zip'

CMAKE_URL='https://github.com/Kitware/CMake/releases/download/v3.21.2/cmake-3.21.2-linux-x86_64.tar.gz'

LANGUAGE_TOOL_URL='https://languagetool.org/download/LanguageTool-stable.zip'

echo '==========================================='
echo '  Checking for required applications...    '
echo '==========================================='

which npm 2>&1 1>/dev/null

if [ $? -ne 0 ]; then
  echo "Please install npm." 1>&2 >&2

  exit 1
fi

which pip3 2>&1 1>/dev/null

if [ $? -ne 0 ]; then
  echo "Please install pip3." 1>&2 >&2

  exit 2
fi

which apt 2>&1 1>/dev/null

if [ $? -ne 0 ]; then
  echo "Only Ubuntu is supported at the moment for clangd." 1>&2 >&2

  exit 3
fi

which go 2>&1 1>/dev/null

if [ $? -ne 0 ]; then
  echo "Please install Golang." 1>&2 >&2

  exit 4
fi

which wget 2>&1 1>/dev/null

if [ $? -ne 0 ]; then
  echo "Please install wget." 1>&2 >&2

  exit 5
fi

which unzip 2>&1 1>/dev/null

if [ $? -ne 0 ]; then
  echo "Please install unzip." 1>&2 >&2

  exit 6
fi

which git 2>&1 1>/dev/null

if [ $? -ne 0 ]; then
  echo "Please install git." 1>&2 >&2

  exit 7
fi

echo '==========================================='
echo '  Installing required dependencies...  '
echo '==========================================='

rm -rf jdt-language-server-latest.tar.gz

rm -rf /usr/local/jdtls

mkdir -p /usr/local/jdtls

wget --timeout=10 "${JDTLS_URL}"

if [ $? -ne 0 ]; then
  echo "Failed to download JDTLS server." 1>&2 >&2

  exit 8
fi

tar xvzf jdt-language-server-latest.tar.gz -C /usr/local/jdtls

if [ $? -ne 0 ]; then
  echo "Failed to unpack JDTLS server archive." 1>&2 >&2

  exit 9
fi

rm -rf jdt-language-server-latest.tar.gz

sudo apt update

if [ $? -ne 0 ]; then
  echo "Failed to update package repository." 1>&2 >&2

  exit 10 
fi

sudo apt install -y clangd

if [ $? -ne 0 ]; then
  echo "Failed to install clangd." 1>&2 >&2

  exit 11 
fi

pip3 install cmake-language-server

if [ $? -ne 0 ]; then
  echo "Failed to install the CMake language server." 1>&2 >&2

  exit 12
fi

GO111MODULE=on go get golang.org/x/tools/gopls@latest

if [ $? -ne 0 ]; then
  echo "Failed to install Gopls." 1>&2 >&2

  exit 13
fi

npm i -g vscode-langservers-extracted

if [ $? -ne 0 ]; then
  echo "Failed to install VSCode LSP." 1>&2 >&2

  exit 14
fi

npm i -g pyright

if [ $? -ne 0 ]; then
  echo "Failed to install Pyright." 1>&2 >&2

  exit 15
fi

npm i -g svelte-language-server

if [ $? -ne 0 ]; then
  echo "Failed to install Svelete LSP." 1>&2 >&2

  exit 16
fi

npm i -g vim-language-server

if [ $? -ne 0 ]; then
  echo "Failed to install Vim LSP." 1>&2 >&2

  exit 17
fi

npm i -g diagnostic-languageserver

if [ $? -ne 0 ]; then
  echo "Failed to install the Diagnostic LSP." 1>&2 >&2

  exit 18
fi

npm i -g bash-language-server

if [ $? -ne 0 ]; then
  echo "Failed to install the BASH LSP." 1>&2 >&2

  exit 19
fi

sudo apt install -y ripgrep

if [ $? -ne 0 ]; then
  echo "Failed to install ripgrep." 1>&2 >&2

  exit 20
fi

rm -rf cpptools-linux.vsix

rm -rf /usr/local/cpptools

wget --timeout=10 "${CPPTOOLS_URL}"

if [ $? -ne 0 ]; then
  echo "Failed to download cpptools." 1>&2 >&2

  exit 21
fi

mv cpptools-linux.vsix cpptools-linux.zip

mkdir -p /usr/local/cpptools

unzip cpptools-linux.zip -d /usr/local/cpptools

if [ $? -ne 0 ]; then
  echo "Failed to unpack cpptools." 1>&2 >&2

  exit 22
fi

chmod +x /usr/local/cpptools/extension/debugAdapters/bin/OpenDebugAD7

cp /usr/local/cpptools/extension/cppdbg.ad7Engine.json /usr/local/cpptools/extension/debugAdapters/bin/nvim-dap.ad7Engine.json

rm -rf cpptools-linux.zip

rm -rf "${OPENJDK_URL##*/}"

wget --timeout=10 "${OPENJDK_URL}"

if [ $? -ne 0 ]; then
  echo "Failed to get OpenJDK." 1>&2 >&2

  exit 23
fi

tar xvzf "${OPENJDK_URL##*/}"

if [ $? -ne 0 ]; then
  echo "Failed to unpack OpenJDK." 1>&2 >&2

  exit 24
fi

mv `find . -maxdepth 1 -name jdk*` /usr/local/jdk

rm -rf "${OPENJDK_URL##*/}"

rm -rf "${GRADLE_URL##*/}"

wget --timeout=10 "${GRADLE_URL}"

if [ $? -ne 0 ]; then
  echo "Failed to get Gradle." 1>&2 >&2

  exit 25
fi

unzip "${GRADLE_URL##*/}" -d /usr/local

if [ $? -ne 0 ]; then
  echo "Failed to unpack Gradle." 1>&2 >&2

  exit 26
fi

mv /usr/local/`echo "${GRADLE_URL##*/}" | cut -d'-' -f-2` /usr/local/gradle

rm -rf "${GRADLE_URL##*/}"

rm -rf 'lombok.jar'

wget --timeout=10 'https://projectlombok.org/downloads/lombok.jar'

if [ $? -ne 0 ]; then
  echo "Failed to download Lombok." 1>&2 >&2

  exit 27
fi

mv 'lombok.jar' /usr/local/jdtls/

pip3 install cmake-format

if [ $? -ne 0 ]; then
  echo "Failed to install cmake-format." 1>&2 >&2

  exit 28
fi

rm -rf "${CMAKE_URL##*/}"

wget --timeout=10 "${CMAKE_URL}"

if [ $? -ne 0 ]; then
  echo "Failed to download CMake." 1>&2 >&2

  exit 29
fi

tar xvzf "${CMAKE_URL##*/}"

if [ $? -ne 0 ]; then
  echo "Failed to unpack CMake." 1>&2 >&2

  exit 30
fi

mv `echo "${CMAKE_URL##*/}" | cut -d '.' -f-3` /usr/local/cmake

rm -rf "${CMAKE_URL##*/}"

sudo apt install -y flex bison build-essential cmake python3 zlib1g zlib1g-dev make binutils bzip2 coreutils grep gzip sed tar unzip zip gcc g++

if [ $? -ne 0 ]; then
  echo "Failed to install gcc/clang dependencies." 1>&2 >&2

  exit 31
fi

wget --timeout=10 "${LANGUAGE_TOOL_URL}"

if [ $? -ne 0 ]; then
  echo "Failed to download LanguageTool." 1>&2 >&2

  exit 32
fi

mkdir -p /usr/local/LanguageTool

unzip LanguageTool-stable.zip

if [ $? -ne 0 ]; then
  echo "Failed to unpack LanguageTool." 1>&2 >&2

  exit 33
fi

LANGUAGE_TOOL_NAME=`ls -l | grep 'LanguageTool-' | grep -v 'zip' | awk '{ print $9 }'`

mv ${LANGUAGE_TOOL_NAME}/* /usr/local/LanguageTool/

rm -rf ${LANGUAGE_TOOL_NAME}

rm -rf LanguageTool-stable.zip

echo '==========================================='
echo '  Installation complete.                   '
echo '==========================================='

exit 0
