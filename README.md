Dev-Setup
=========

This repository contains various configuration and scripts for setting up a development machine.

Requirements
------------

The scripts and configuration in this project require the following programs (they will notify
you if one isn't installed):

* BASH
  * The shell used on the system and by the scripts.
* curl
  * To download web content, such as parsing for the `go` download file.
* wget
  * To download applications from the web.
* git
  * To checkout repositories required by the scripts.
* go
  * Required for neovim setup. Use the [install\_go.sh](install_go.sh) script to do this.
* node
  * Required for neovim setup. Use the [install\_node.sh](install_node.sh) script to do this.
* nvim
  * Neovim is the editor of choice so this must be installed. Use the [install\_nvim.sh](install_nvim.sh) script to do this.
* python3/pip3
  * Neovim requires python3, as well as some of the other tooling for cmake. Use the [install\_python.sh](install_python.sh) script to do this.
* cmake
  * A build tool for C/C++ code.
* clang/clang++
  * A LLVM compiler for C/C++.
* clang-tidy
  * A very good linting tool for C/C++ code.
* clang-format
  * A very good formatting tool for C/C++ code.

It is assumed that this repository will be checked out into `/usr/local/src/dev-setup`. If you need
to setup proper privliges for `/usr/local`, use the following commands:

    sudo chown -R root:sudo /usr/local
    
    sudo chmod -R g+rw /usr/local

Files
-----

The following files are available:

* [bash.source](bash.source)
  * This file is sourced for bash in order to pull in bash configuration. Note that in order for this to work,
    you must source this file into your `${HOME}/.bashrc` file using the following line:

        source /usr/local/src/dev-setup/bash.source

    Once that is added it will automatically source the file the next time you open a terminal.
* [jdtls\_launcher.sh](jdtls_launcher.sh)
  * This file is used to launch jdtls (must run [install\_dependencies.sh](install_dependencies.sh) first).
    Afterwards, run the following to add it to the PATH:

        ln -s /usr/local/src/dev-setup/jdtls_launcher.sh /usr/local/sbin/jdtls_launcher.sh

* [init.lua](init.lua)
  * This file is the _ini_ file for Neovim.
* [lua](lua/)
  * These files are extra scripts used for [init.lua](init.lua).
* [install\_go.sh](install_go.sh)
  * This file will download the latest stable `go` tooling.
* [install\_nvim.sh](install_nvim.sh)
  * This will install neovim.
* [install\_dependencies.sh](install_dependencies.sh)
  * This will install all required language servers for neovim.
* [initialize\_nvim.sh](initialize_nvim.sh)
  * This will initialze the neovim environment.
* [install\_node.sh](install_node.sh)
  * This file will download the latest LTS `node` tooling.
* [install\_python.sh](install_python.sh)
  * This file will download the latest python tooling.
* [install\_rust.sh](install_rust.sh)
  * This file will download the latest `rust` tooling.
* [install\_nerd\_fonts.sh](install_nerd_fonts.sh)
  * This will install the Fira nerd fonts.
* [system\_update.sh](system_update.sh)
  * This script will update your Linux system (works with Ubuntu, RedHat/Fedora, and Arch).
* [install\_gcc.sh](install_gcc.sh)
  * This file will build the latest bleeding edge GNU compiler suite.
* [install\_llvm.sh](install_llvm.sh)
  * This file will build the latest bleeding edge LLVM compiler/tool suite.
