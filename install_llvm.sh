#!/bin/bash

GIT_URL='https://github.com/llvm/llvm-project.git'

DEPENDENCIES='flex bison build-essential cmake python3 zlib1g zlib1g-dev make binutils bzip2 coreutils grep gzip sed tar unzip zip gcc g++'

INSTALL_DIR='/usr/local/toolchains'

MAKE_CORE_NUM='16'

echo '========================================='
echo '  Checking out repository...             '
echo '========================================='

git clone "${GIT_URL}" llvm

CURRENT_DIR="${PWD}"

cd llvm

echo '========================================='
echo '  Configuring LLVM...                         '
echo '========================================='

cmake -S llvm -B build -G "Unix Makefiles" -DLLVM_ENABLE_PROJECTS="clang;clang-tools-extra;compiler-rt;lld;lldb;openmp" -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" -DCMAKE_BUILD_TYPE=Release

cd build/

echo '========================================='
echo '  Building LLVM...                            '
echo '========================================='

make -j${MAKE_CORE_NUM}

echo '========================================='
echo '  Installing LLVM...                          '
echo '========================================='

make install

cd "${CURRENT_DIR}"

rm -rf llvm

echo '========================================='
echo '  Installation complete.                 '
echo '========================================='

exit 0
