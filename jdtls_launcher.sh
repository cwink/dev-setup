#!/bin/bash

export JAVA_HOME='/usr/local/jdk'

export JAR='/usr/local/jdtls/plugins/org.eclipse.equinox.launcher_*.jar'

export JAR_CONFIG='/usr/local/jdtls/config_linux'

export GRADLE_HOME='/usr/local/gradle'

export PATH="${JAVA_HOME}/bin:${GRADLE_HOME}/bin:${PATH}"

mkdir -p ${HOME}/.local/share/nvim/workspace

java                                                \
  -Declipse.application=org.eclipse.jdt.ls.core.id1 \
  -Dosgi.bundles.defaultStartLevel=4                \
  -Declipse.product=org.eclipse.jdt.ls.core.product \
  -Dlog.protocol=true                               \
  -Dlog.level=ALL                                   \
  -javaagent:/usr/local/jdtls/lombok.jar            \
  -Xms1G                                            \
  -Xmx2G                                            \
  -jar `echo "${JAR}"`                              \
  -configuration "${JAR_CONFIG}"                    \
  -data "${1:-$HOME/.local/share/nvim/workspace}" \
  --add-modules=ALL-SYSTEM                          \
  --add-opens java.base/java.util=ALL-UNNAMED       \
  --add-opens java.base/java.lang=ALL-UNNAMED
