#!/bin/bash

GIT_URL='git://gcc.gnu.org/git/gcc.git'

INSTALL_DIR='/usr/local/toolchains'

LANGUAGES='c,c++'

MAKE_CORE_NUM='16'

echo '========================================='
echo '  Checking out repository...             '
echo '========================================='

git clone "${GIT_URL}" gcc

CURRENT_DIR="${PWD}"

cd gcc

echo '========================================='
echo '  Downloading prerequisites...           '
echo '========================================='

contrib/download_prerequisites 

mkdir build

cd build

echo '========================================='
echo '  Configuring...                         '
echo '========================================='

../configure -v --prefix="${INSTALL_DIR}" --enable-languages="${LANGUAGES}" --disable-multilib

echo '========================================='
echo '  Building...                            '
echo '========================================='

make -j${MAKE_CORE_NUM}

echo '========================================='
echo '  Installing...                          '
echo '========================================='

make install-strip

cd "${CURRENT_DIR}"

rm -rf gcc

echo '========================================='
echo '  Installation complete.                 '
echo '========================================='

exit 0
