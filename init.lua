require('cw.filesystem')

require('cw.plugins')

vim.api.nvim_set_keymap('n', 'gw', ':wa!<cr>', { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', 'gq', ':qa!<cr>', { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '>', ':bn!<cr>', { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<', ':bp!<cr>', { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', 'gd', ':bd!<cr>', { noremap = true, silent = true })

vim.o.nu = true

vim.o.ignorecase = false

vim.o.wrap = false

vim.o.ts = 8

vim.o.sts = 8

vim.o.sw = 8

vim.o.expandtab = false

vim.o.errorbells = false

vim.o.autoindent = true

vim.o.smartindent = true

vim.cmd('set sessionoptions-=blank')

vim.g.languagetool_server_jar = '/usr/local/LanguageTool/languagetool-server.jar'
