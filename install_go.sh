#!/bin/bash

BASE_URL='https://golang.org'

TEMP_FILE='.download.tmp'

echo '========================================='
echo '  Checking for required applications...  '
echo '========================================='

which 'curl'

if [ $? -ne 0 ]; then
  echo "Please install curl." 1>&2 >&2

  exit 1
fi

which 'wget'

if [ $? -ne 0 ]; then
  echo "Please install wget." 1>&2 >&2

  exit 2
fi

which 'tar'

if [ $? -ne 0 ]; then
  echo "Please install tar." 1>&2 >&2

  exit 3
fi

echo '========================================='
echo '  Downloading installation file...       '
echo '========================================='

curl -kLs --connect-timeout 10 "${BASE_URL}/dl" >"${TEMP_FILE}"

if [ $? -ne 0 ]; then
  rm -rf "${TEMP_FILE}"

  echo "Failed to connect to ${BASE_URL}." 1>&2 >&2

  exit 4
fi

DOWNLOAD_SUFFIX=`cat ${TEMP_FILE} | grep -B1 '<div class="platform">Linux</div>' | grep -Eo 'href="[^"]*"' | cut -d'"' -f2`

if [ "${DOWNLOAD_SUFFIX}" == "" ]; then
  rm -rf "${TEMP_FILE}"

  echo "Failed to obtain download url." 1>&2 >&2

  exit 5
fi

DOWNLOAD_URL="${BASE_URL}${DOWNLOAD_SUFFIX}"

rm -rf "${TEMP_FILE}"

wget --timeout=10 "${DOWNLOAD_URL}"

if [ $? -ne 0 ]; then
  echo "Failed to download file." 1>&2 >&2

  exit 6
fi

echo '========================================='
echo '  Installing go...                       '
echo '========================================='

INSTALL_FILE=`echo "${DOWNLOAD_URL}" | cut -d'/' -f5`

rm -rf /usr/local/go && tar -C /usr/local -xzvf "${INSTALL_FILE}"

if [ $? -ne 0 ]; then
  rm -rf "${INSTALL_FILE}"

  echo "Failed to install go." 1>&2 >&2

  exit 7
fi

rm -rf "${INSTALL_FILE}"

echo '========================================='
echo '  Installation complete.                 '
echo '========================================='

exit 0
