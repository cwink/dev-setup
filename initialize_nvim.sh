#!/bin/bash

echo '========================================='
echo '  Checking for required applications...  '
echo '========================================='

which nvim

if [ $? -ne 0 ]; then
  echo "Please install neovim." 1>&2 >&2

  exit 1
fi

echo '========================================='
echo '  Preping nveovim configuration...       '
echo '========================================='

rm -rf "${HOME}/.local/share/nvim"

mkdir -p "${HOME}/.local/share/nvim"

rm -rf "${HOME}/.config/nvim"

mkdir -p "${HOME}/.config/nvim"

ln -s "/usr/local/src/dev-setup/init.lua" "${HOME}/.config/nvim/init.lua"

ln -s "/usr/local/src/dev-setup/lua" "${HOME}/.config/nvim/lua"

echo '========================================='
echo '  Installing neovim plugins...           '
echo '========================================='

git clone --depth 1 https://github.com/wbthomason/packer.nvim ~/.local/share/nvim/site/pack/packer/start/packer.nvim

nvim --headless -c 'autocmd User PackerComplete quitall' -c 'PackerSync'

if [ $? -ne 0 ]; then
  echo 'Failed to install neovim plugins.' 1>&2 >&2

  exit 2
fi

echo ''
echo '========================================='
echo '  Installation complete.                 '
echo '========================================='

exit 0
