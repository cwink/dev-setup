#!/bin/bash

BASE_URL='https://nodejs.org'

TEMP_FILE='.download.tmp'

echo '========================================='
echo '  Checking for required applications...  '
echo '========================================='

which curl 2>&1 1>/dev/null

if [ $? -ne 0 ]; then
  echo "Please install curl." 1>&2 >&2

  exit 1
fi

which wget 2>&1 1>/dev/null

if [ $? -ne 0 ]; then
  echo "Please install wget." 1>&2 >&2

  exit 2
fi

which tar 2>&1 1>/dev/null

if [ $? -ne 0 ]; then
  echo "Please install tar." 1>&2 >&2

  exit 3
fi

echo '========================================='
echo '  Downloading installation file...       '
echo '========================================='

curl -kLs --connect-timeout 10 "${BASE_URL}/en/download/" >"${TEMP_FILE}"

if [ $? -ne 0 ]; then
  rm -rf "${TEMP_FILE}"

  echo "Failed to connect to ${BASE_URL}." 1>&2 >&2

  exit 4
fi

DOWNLOAD_URL=`cat ${TEMP_FILE} | grep -A1 'Linux Binaries (x64)' | grep -Eo 'href="[^"]*"' | cut -d'"' -f2`

rm -rf "${TEMP_FILE}"

wget --timeout=10 "${DOWNLOAD_URL}"

if [ $? -ne 0 ]; then
  echo "Failed to download file." 1>&2 >&2

  exit 5
fi

echo '========================================='
echo '  Installing node...                     '
echo '========================================='

INSTALL_FILE=`echo "${DOWNLOAD_URL}" | cut -d'/' -f6-`

rm -rf /usr/local/node && tar -C /usr/local -xJvf "${INSTALL_FILE}"

if [ $? -ne 0 ]; then
  rm -rf "${INSTALL_FILE}"

  echo "Failed to install node." 1>&2 >&2

  exit 6
fi

rm -rf "${INSTALL_FILE}"

DIRECTORY_NAME=`echo "${INSTALL_FILE}" | cut -d'.' -f-3`

mv "/usr/local/${DIRECTORY_NAME}" "/usr/local/node"

echo '========================================='
echo '  Installation complete.                 '
echo '========================================='

exit 0
