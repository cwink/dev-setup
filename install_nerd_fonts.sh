#!/bin/bash

echo '========================================='
echo '  Checking for required applications...  '
echo '========================================='

which wget 2>&1 1>/dev/null

if [ $? -ne 0 ]; then
  echo "Please install git." 1>&2 >&2

  exit 1
fi

echo '========================================='
echo '  Installing fonts...                    '
echo '========================================='

wget --timeout=10 'https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FiraCode.zip'

rm -rf fonts

mkdir -p fonts

cd fonts

unzip ../FiraCode.zip 

mkdir -p /usr/local/share/fonts

mv *.otf /usr/local/share/fonts

cd -

rm -rf fonts

rm -rf FiraCode.zip

fc-cache -f -v

echo '========================================='
echo '  Installation complete.                 '
echo '========================================='

exit 0
