#!/bin/bash

echo '========================================='
echo '  Checking for required applications...  '
echo '========================================='

which apt 2>&1 1>/dev/null

if [ $? -ne 0 ]; then
  echo "Only Ubuntu is supported at the moment for dependencies." 1>&2 >&2

  exit 1
fi

which git 2>&1 1>/dev/null

if [ $? -ne 0 ]; then
  echo "Please install git." 1>&2 >&2

  exit 2
fi

echo '========================================='
echo '  Installing dependencies...             '
echo '========================================='

sudo apt update

if [ $? -ne 0 ]; then
  echo "Failed to update package respository." 1>&2 >&2

  exit 3
fi

sudo apt install -y build-essential gdb lcov libbz2-dev libffi-dev libgdbm-dev liblzma-dev libncurses5-dev libreadline6-dev libsqlite3-dev libssl-dev lzma lzma-dev tk-dev uuid-dev zlib1g-dev

if [ $? -ne 0 ]; then
  echo "Failed to install all dependencies." 1>&2 >&2

  exit 4
fi

rm -rf /usr/local/neovim

rm -rf neovim

echo '========================================='
echo '  Checking out repository...             '
echo '========================================='

git clone https://github.com/python/cpython.git

if [ $? -ne 0 ]; then
  echo "Failed to checkout python repository." 1>&2 >&2

  exit 5
fi

cd cpython/

echo '========================================='
echo '  Building Python...                     '
echo '========================================='

./configure --with-pydebug --prefix=/usr/local/python

if [ $? -ne 0 ]; then
  echo "Failed to configure Python for build." 1>&2 >&2

  exit 6
fi

make -s -j2

if [ $? -ne 0 ]; then
  echo "Failed to build Python." 1>&2 >&2

  exit 7
fi

make install

if [ $? -ne 0 ]; then
  echo "Failed to install Python." 1>&2 >&2

  exit 8
fi

cd ..

rm -rf cpython

echo '========================================='
echo '  Installation complete.                 '
echo '========================================='

exit 0
