vim.g.solarized_termtrans = 1

vim.o.termguicolors = true

vim.o.background = 'dark'

vim.api.nvim_command('syntax on')

vim.api.nvim_command('colorscheme solarized')
