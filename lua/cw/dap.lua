local dap = require('dap')

dap.adapters.lldb = {
  type = 'executable',
  command = 'lldb-vscode',
  name = 'lldb'
}

dap.configurations.c = {
  {
    name = "Debug",
    type = "lldb",
    request = "launch",
    program = function()
      return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
    end,
    cwd = '${workspaceFolder}',
    terminal = 'integrated'
  }
}

dap.configurations.cpp = dap.configurations.c

vim.api.nvim_set_keymap('n', '<leader>dd', ":lua require('dap').continue()<cr>", { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>do', ":lua require('dap').step_over()<cr>", { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>di', ":lua require('dap').step_into()<cr>", { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>dt', ":lua require('dap').step_out()<cr>", { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>dbp', ":lua require('dap').toggle_breakpoint()<cr>", { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>dbc', ":lua require('dap').set_breakpoint(vim.fn.input('Breakpoint condition: '))<cr>", { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>dh', ":lua require('dap.ui.variables').hover()<cr>", { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>ds', ":lua require('dap.ui.variables').scopes()<cr>", { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>dc', ":lua require('telescope').extensions.dap.commands()<cr>", { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>dl', ":lua require('telescope').extensions.dap.list_breakpoints()<cr>", { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>dv', ":lua require('telescope').extensions.dap.variables()<cr>", { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>dr', ":lua require('dap.repl').open()<cr>", { noremap = true, silent = true })

vim.g.dap_virtual_text = true
