local lsp = require('lspconfig')

local buffer_map = function(type, key, value)
  vim.api.nvim_buf_set_keymap(0, type, key, value, { noremap = true, silent = true })
end

local function custom_on_attach(client)
  buffer_map('n', '<leader>gdc', '<cmd>lua vim.lsp.buf.declaration()<cr>')

  buffer_map('n', '<leader>gdf', '<cmd>lua vim.lsp.buf.definition()<cr>')

  buffer_map('n', '<leader>gh', '<cmd>lua vim.lsp.buf.hover()<cr>')

  buffer_map('n', '<leader>grf', '<cmd>lua vim.lsp.buf.references()<cr>')

  buffer_map('n', '<leader>gsh', '<cmd>lua vim.lsp.buf.signature_help()<cr>')

  buffer_map('n', '<leader>gi', '<cmd>lua vim.lsp.buf.implementation()<cr>')

  buffer_map('n', '<leader>gt', '<cmd>lua vim.lsp.buf.type_definition()<cr>')

  buffer_map('n', '<leader>gsd', '<cmd>lua vim.lsp.buf.document_symbol()<cr>')

  buffer_map('n', '<leader>gsw', '<cmd>lua vim.lsp.buf.workspace_symbol()<cr>')

  buffer_map('n', '<leader>gc', '<cmd>lua vim.lsp.buf.code_action()<cr>')

  buffer_map('n', '<leader>gl', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<cr>')

  buffer_map('n', '<leader>gn', '<cmd>lua vim.lsp.diagnostic.goto_next()<cr>')

  buffer_map('n', '<leader>gp', '<cmd>lua vim.lsp.diagnostic.goto_prev()<cr>')

  buffer_map('n', '<leader>grn', '<cmd>lua vim.lsp.buf.rename()<cr>')

  buffer_map('n', '<leader>gf', '<cmd>lua vim.lsp.buf.format({ async = true })<cr>')

  require('lsp_signature').on_attach()

  vim.cmd([[
    augroup AUTOFORMAT
      autocmd!
      autocmd BufWritePre <buffer> :lua vim.lsp.buf.formatting_seq_sync(nil, 1000)
    augroup END
  ]])
end

lsp.clangd.setup {
  cmd = { 'clangd', '--background-index', '--clang-tidy' },
  on_attach = custom_on_attach
}

lsp.cmake.setup {
  on_attach = custom_on_attach
}

lsp.gopls.setup {
  on_attach = custom_on_attach
}

lsp.html.setup {
  on_attach = custom_on_attach
}

lsp.pyright.setup {
  on_attach = custom_on_attach
}

lsp.rls.setup {
  on_attach = custom_on_attach
}

lsp.svelte.setup {
  on_attach = custom_on_attach
}

lsp.vimls.setup {
  on_attach = custom_on_attach
}

lsp.diagnosticls.setup {
  on_attach = custom_on_attach
}

lsp.bashls.setup {
  on_attach = custom_on_attach
}

vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
  vim.lsp.diagnostic.on_publish_diagnostics, {
    underline = true,

    virtual_text = {
      spacing = 4,

      prefix = '→'
    }
  }
)

vim.fn.sign_define("LspDiagnosticsSignError", {text = "✖", numhl = "LspDiagnosticsDefaultError"})

vim.fn.sign_define("LspDiagnosticsSignWarning", {text = "⚠", numhl = "LspDiagnosticsDefaultWarning"})

vim.fn.sign_define("LspDiagnosticsSignInformation", {text = "ⓘ", numhl = "LspDiagnosticsDefaultInformation"})

vim.fn.sign_define("LspDiagnosticsSignHint", {text = "☛", numhl = "LspDiagnosticsDefaultHint"})

vim.cmd([[
  augroup JDTLS
    autocmd!
    autocmd FileType java lua require('jdtls').start_or_attach({ cmd = { 'jdtls_launcher.sh' }, root_dir = require('jdtls.setup').find_root({ 'gradle.build', 'pom.xml' })})
    autocmd FileType java nnoremap <leader>gdc :lua vim.lsp.buf.declaration()<cr>
    autocmd FileType java nnoremap <leader>gdf :lua vim.lsp.buf.definition()<cr>
    autocmd FileType java nnoremap <leader>gh :lua vim.lsp.buf.hover()<cr>
    autocmd FileType java nnoremap <leader>grf :lua vim.lsp.buf.references()<cr>
    autocmd FileType java nnoremap <leader>gsh :lua vim.lsp.buf.signature_help()<cr>
    autocmd FileType java nnoremap <leader>gi :lua vim.lsp.buf.implementation()<cr>
    autocmd FileType java nnoremap <leader>gt :lua vim.lsp.buf.type_definition()<cr>
    autocmd FileType java nnoremap <leader>gsd :lua vim.lsp.buf.document_symbol()<cr>
    autocmd FileType java nnoremap <leader>gsw :lua vim.lsp.buf.workspace_symbol()<cr>
    autocmd FileType java nnoremap <leader>gc :lua require('jdtls').code_action()<cr>
    autocmd FileType java nnoremap <leader>gre :lua require('jdtls').code_action(false, 'refactor')<cr>
    autocmd FileType java nnoremap <leader>go :lua require('jdtls').organize_imports()<cr>
    autocmd FileType java nnoremap <leader>gl :lua vim.lsp.diagnostic.show_line_diagnostics()<cr>
    autocmd FileType java nnoremap <leader>gn :lua vim.lsp.diagnostic.goto_next()<cr>
    autocmd FileType java nnoremap <leader>gp :lua vim.lsp.diagnostic.goto_prev()<cr>
    autocmd FileType java nnoremap <leader>grn :lua vim.lsp.buf.rename()<cr>
    autocmd FileType java nnoremap <leader>gf :lua vim.lsp.buf.formatting()<cr>
  augroup END
]])
