local directories = {
  vim.fn.stdpath('data')..'/backup',
  vim.fn.stdpath('data')..'/swap',
  vim.fn.stdpath('data')..'/undo'
}

for _, directory in ipairs(directories) do
  vim.fn.mkdir(directory, 'p')
end

vim.o.backupdir = directories[1]
vim.o.directory = directories[2]
vim.o.undodir = directories[3]

vim.o.undofile = true
vim.o.backup = true
