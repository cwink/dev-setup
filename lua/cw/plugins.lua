local packer_install_path = vim.fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'

if vim.fn.empty(vim.fn.glob(packer_install_path)) > 0 then
  vim.api.nvim_command('!git clone --depth 1 https://github.com/wbthomason/packer.nvim '..packer_install_path)

  vim.api.nvim_command('packadd packer.nvim')
end

return require('packer').startup(function()
  use 'wbthomason/packer.nvim'

  use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate', config = function() require('cw.treesitter') end }

  use { 'kyazdani42/nvim-tree.lua', requires = { 'kyazdani42/nvim-web-devicons' }, config = function() require('cw.tree') end }

  use { 'ishan9299/nvim-solarized-lua', config = function() require('cw.colors') end }

  use { 'neovim/nvim-lspconfig', requires = { 'mfussenegger/nvim-jdtls' }, config = function() require('cw.lsp') end }

  use { 'hrsh7th/nvim-cmp', requires = { 'hrsh7th/vim-vsnip', 'hrsh7th/cmp-buffer', 'hrsh7th/cmp-nvim-lsp', 'ray-x/lsp_signature.nvim' }, config = function() require('cw.cmp') end }

  use { 'hoob3rt/lualine.nvim', requires = { 'kyazdani42/nvim-web-devicons', 'kdheepak/tabline.nvim' }, config = function() require('cw.line') end }

  use { 'nvim-telescope/telescope.nvim', requires = { 'nvim-lua/plenary.nvim' }, config = function() require('cw.telescope') end }

  use { 'mfussenegger/nvim-dap', requires = { 'nvim-telescope/telescope-dap.nvim', 'theHamsta/nvim-dap-virtual-text' }, config = function() require('cw.dap') end }

  use { 'simrat39/symbols-outline.nvim', config = function() require('cw.outline') end }
end)
