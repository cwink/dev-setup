vim.api.nvim_set_keymap('n', '<C-n>', ':NvimTreeToggle<cr>', { noremap = true, silent = true })

local nvtree = require('nvim-tree').setup {}
