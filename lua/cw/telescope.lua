require('telescope').setup()

require('telescope').load_extension('dap')

vim.api.nvim_set_keymap('n', '<leader>tgo', ':Telescope live_grep grep_open_files=true<cr>', { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>tga', ':Telescope live_grep<cr>', { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>tb', ':Telescope buffers<cr>', { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>th', ':Telescope help_tags<cr>', { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>tt', ':Telescope treesitter<cr>', { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>tff', ':Telescope find_files<cr>', { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>tfb', ':Telescope file_browser<cr>', { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>tm', ':Telescope marks<cr>', { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>tr', ':Telescope lsp_references<cr>', { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>ts', ':Telescope lsp_document_symbols<cr>', { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>tsw', ':Telescope lsp_dynamic_workspace_symbols<cr>', { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>td', ':Telescope lsp_workspace_diagnostics<cr>', { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>tk', ':Telescope keymaps<cr>', { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<leader>tc', ':Telescope commands<cr>', { noremap = true, silent = true })
