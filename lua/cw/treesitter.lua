local treesitter_configs = require('nvim-treesitter.configs')

treesitter_configs.setup { 
  highlight = {
    enable = true,

    additional_vim_regex_highlighting = true
  },
  indent = {
    enable = true
  },
  ensure_installed = {
    "bash",
    "c",
    "cmake",
    "cpp",
    "css",
    "dockerfile",
    "go",
    "html",
    "java",
    "javascript",
    "json",
    "latex",
    "lua",
    "python",
    "regex",
    "rust",
    "ruby",
    "scss",
    "svelte",
    "toml",
    "typescript",
    "vim",
    "vue",
    "yaml",
    "zig"
  }
}

vim.cmd([[
  augroup TSErrorNormal
    autocmd!
    autocmd BufEnter <buffer> highlight! link TSError Normal
  augroup END
]])
